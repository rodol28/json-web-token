const express = require('express');
const jwt = require('jsonwebtoken');
const app = express();

//settings
app.set('port', process.env.PORT || 3000);

//routes
app.get('/', (req, res) => {
    res.json({
        msg: 'api works!'
    });
});

app.post('/api/login', (req, res) => {
    const user = {
        id: 3
    };
    const token = jwt.sign({
        user
    }, 'my_secret_key');
    res.json({
        token
    });
});

app.get('/api/protected', ensureToken, (req, res) => {
    jwt.verify(req.token, 'my_secret_key', (err, data) => {
        if (err) {
            res.sendStatus(403);
        } else {
            res.json({
                text: 'protected', 
                data: data
            });
        }
    });
});

//middleware
function ensureToken(req, res, next) {
    const bearerHeader = req.headers['authorization'];
    console.log(bearerHeader);
    if (typeof bearerHeader != 'undefined') {
        const bearer = bearerHeader.split(' ');
        const bearerToken = bearer[1];
        req.token = bearerToken;
        next();
    } else {
        res.sendStatus(403);
    }
}


//start the server
app.listen(app.get('port'), () => {
    console.log(`Server listening on port ${app.get('port')}`);
});